/*
 * Copyright (C) 2018 Esteban López | gnu_stallman (at) protonmail (dot) ch
 *
 * This file is part of TER
 *
 * This program is free software: you can redistribute it and/or modify
 * it under the terms of the GNU General Public License as published by
 * the Free Software Foundation, either version 3 of the License, or
 * (at your option) any later version.
 *
 *
 * This program is distributed in the hope that it will be useful,
 * but WITHOUT ANY WARRANTY; without even the implied warranty of
 * MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
 * GNU General Public License for more details.
 *
 * You should have received a copy of the GNU General Public License
 * along with this program.  If not, see http://www.gnu.org/licenses/.
 */

package gui;

import backend.GameMode;
import java.awt.*;
import java.awt.event.ActionEvent;
import java.awt.event.ActionListener;

import javax.swing.*;

public class GameSelector extends JDialog implements ActionListener{
	private JButton next, exit;
	private JRadioButton h_a, h_h, a_a;
	
	public GameSelector() {
		super(new JFrame(), "Selection of game mode", true);
		position(300, 200);
		
		next = new JButton("Next");
		exit = new JButton("Exit");
		next.addActionListener(this);
		exit.addActionListener(this);
		
		ButtonGroup group = new ButtonGroup();
		h_a = new JRadioButton("Human vs AI");
		h_h = new JRadioButton("Human vs human");
		a_a = new JRadioButton("AI vs AI");
		group.add(h_a);
		group.add(h_h);
		group.add(a_a);
		h_a.setSelected(true);
		
		Box buttons = Box.createHorizontalBox(), south = Box.createVerticalBox();
		buttons.add(Box.createGlue());
		buttons.add(next);
		buttons.add(Box.createGlue());
		buttons.add(exit);
		buttons.add(Box.createGlue());
		south.add(buttons);
		south.add(Box.createVerticalStrut(20));
		
		Box labelRadioButtons = Box.createVerticalBox(), center = Box.createHorizontalBox();
		labelRadioButtons.add(new JLabel("Select a game mode:"));
		labelRadioButtons.add(Box.createVerticalStrut(7));
		labelRadioButtons.add(h_a);
		labelRadioButtons.add(h_h);
		labelRadioButtons.add(a_a);
		center.add(Box.createGlue());
		center.add(labelRadioButtons);
		center.add(Box.createGlue());
		
		add(center, BorderLayout.CENTER);
		add(south, BorderLayout.SOUTH);
		setVisible(true);
	}
	
	/**
	 * Retrieves the game mode selected
	 * @return game mode selected
	 */
	public GameMode getSelection() {
		if (h_a.isSelected()) {
			return GameMode.HUMAN_AI;
		} else if (h_h.isSelected()) {
			return GameMode.HUMAN_HUMAN;
		} else {
			return GameMode.AI_AI;
		}
	}
	
	/**
	 * Sets the frame's size with centered position
	 * 
	 * @param sizeX width
	 * @param sizeY height
	 */
	private void position(int sizeX, int sizeY) {
		Dimension screen = Toolkit.getDefaultToolkit().getScreenSize();
		int marginX, marginY;
		
		marginX = (screen.width - sizeX) / 2;
		marginY = (screen.height - sizeY) / 2;
		setBounds(marginX, marginY, sizeX, sizeY);
	}

	@Override
	public void actionPerformed(ActionEvent arg0) {
		if (arg0.getSource().equals(exit))
			System.exit(0);
		else {
			dispose();
			getOwner().dispose();
		}
	}

}
