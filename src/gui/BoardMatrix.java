/*
 * Copyright (C) 2018 Esteban López | gnu_stallman (at) protonmail (dot) ch
 *
 * This file is part of TER
 *
 * This program is free software: you can redistribute it and/or modify
 * it under the terms of the GNU General Public License as published by
 * the Free Software Foundation, either version 3 of the License, or
 * (at your option) any later version.
 *
 *
 * This program is distributed in the hope that it will be useful,
 * but WITHOUT ANY WARRANTY; without even the implied warranty of
 * MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
 * GNU General Public License for more details.
 *
 * You should have received a copy of the GNU General Public License
 * along with this program.  If not, see http://www.gnu.org/licenses/.
 */

package gui;

import backend.*;
import javax.swing.*;
import java.awt.*;
import java.awt.event.*;

public class BoardMatrix extends JPanel implements ActionListener {
	private JButton[][] matrix;
	private Game game;
	
	public BoardMatrix(Game game) {
		this.game = game;
		matrix = new JButton[3][3];
		
		setMaximumSize(new Dimension(150, 150));
		setMinimumSize(getMaximumSize());
		
		setLayout(new GridLayout(3, 3));
		for (int i = 0; i <= 2; i++) {
			for (int j = 0; j <= 2; j++) {
				matrix[i][j] = new JButton();
				matrix[i][j].addActionListener(this);
				add(matrix[i][j]);
			}
		}		
		
		updateBoard(null);
	}

	/**
	 * Updates the buttons taking new states from game
	 */
	public void updateBoard(WinningCombination wc) {
		Piece[][] board = game.getBoard().getMatrix();
		Position p;
		int index;

		for (int i = 0; i <= 2; i++) {
			for (int j = 0; j <= 2; j++) {
				switch (board[i][j]) {
				case X:
					matrix[i][j].setText("X");
					matrix[i][j].setEnabled(false);
					break;
				case O:
					matrix[i][j].setText("O");
					matrix[i][j].setEnabled(false);
					break;
				default:
					matrix[i][j].setText("");
					matrix[i][j].setEnabled(wc == null && !game.isAIvsAI());
				}
				matrix[i][j].setBackground(null);
			}
		}

        if (wc != null) {
        	// History display
        	index = 1;
			p = game.getHistory().pollFirst();
			while (p != null) {
				matrix[p.getRow()][p.getColumn()].setText(
						matrix[p.getRow()][p.getColumn()].getText() + String.valueOf(index)
				);
				index++;
				p = game.getHistory().pollFirst();
			}

        	// Winning combination highlighting
        	if (!wc.isDraw()) {
				for (Position pos : wc.getPositions())
					matrix[pos.getRow()][pos.getColumn()].setText(
							"<HTML></U>" + matrix[pos.getRow()][pos.getColumn()].getText() + "</U></HTML>"
					);
			}
		}
	}

	/**
	 * Change a position's background
	 * @param suggested
	 */
	public void suggestPosition(Position suggested) {
		matrix[suggested.getRow()][suggested.getColumn()].setBackground(Color.CYAN);
	}

	@Override
	public void actionPerformed(ActionEvent arg0) {
		for (int i = 0; i <= 2; i++)
			for (int j = 0; j <= 2; j++)
				if (arg0.getSource().equals(matrix[i][j]))
					updateBoard(game.move(new Position(i, j)));
	}
}
